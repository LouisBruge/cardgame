#ifndef HAND_H
#define HAND_H

#include <list>
#include <string>
#include <queue>
#include "Card.h"

class Player {
  private: 
    std::queue<Card, std::list<Card>> _cards;
    std::string _name;

  public:
    explicit Player(const std::string &name);
    std::string name();
    bool hadLoose();
    Card pick();
    void add(std::list<Card>);
    void add(Card);
};

#endif
