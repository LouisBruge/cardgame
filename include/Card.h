#ifndef CARD_H
#define CARD_H

#include <iostream>
enum class SUITE: int {
  HEART,
  CLOVER,
  SPADE,
  DIAMONDS
};

std::ostream &operator << (std::ostream &o, const  SUITE &s);

enum class RANK: int {
  TWO,
  THREE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  HEIGTH,
  NINE,
  TEEN,
  VALET,
  QUEEN,
  KING,
  ACE
};

std::ostream &operator << (std::ostream &o, const RANK &r);

struct Card {
  RANK rank;
  SUITE suite;
};

#endif
