#ifndef DECK_H
#define DECK_H
#include <vector>
#include "Card.h"

class Deck {
  private:
    std::vector<Card> _cards;

  public:
    Deck();
    std::vector<Card> cards();
    void shuffle();
    Card pick();
    bool empty();
};


#endif
