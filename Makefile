EXEC=./bin/game
SRC_DIR=./src
INCLUDE_DIR=./include

MKDIR=mkdir -p
RM=rm -r -v -f

CXX=g++
CXXFLAGS=-Wall -Werror -Wextra -std=c++17 -I $(INCLUDE_DIR)
LDXXFLAGS=

SRC:=$(wildcard $(SRC_DIR)/*.cpp)
OBJ:=$(SRC:.cpp=.o)

.SUFFIXES:=*.o *.cpp

all: $(EXEC)

$(EXEC): $(OBJ)
	$(MKDIR) $(@D)	
	$(CXX) -o $@ $^ $(LDXXFLAGS)

tags: $(SRC)
	ctags -R

docs: $(SRC)
	doxygen Doxyfile

.PHONY:=cppcheck
cppcheck:
	cppcheck --error-exitcode=1 --force -I include --enable="all" --suppress=missingIncludeSystem src

.PHONY:=dev
dev: $(EXEC)
	$(EXEC)

.PHONY:=clean
clean:
	$(RM) $(EXEC)

.PHONY:=mrpropre
mrpropre: clean
	$(RM) $(OBJ)
