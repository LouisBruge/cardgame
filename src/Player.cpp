#include "Player.h"

Player::Player(const std::string &name) : _name(name)
{
}

std::string Player::name()
{
  return _name;
}

Card Player::pick()
{ 
  Card card = _cards.front();
  _cards.pop();
  std::cout << name() << "'s deck size: " << _cards.size() << std::endl;
  return card;
}

void Player::add(Card card) 
{
  _cards.push(card);
}

void Player::add(std::list<Card> cards)
{
  for(auto it = cards.begin(); it != cards.end(); it++) {
    _cards.push(*it);
  }
}

bool Player::hadLoose()
{
  return _cards.size() == 0;
}
