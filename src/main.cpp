#include <iostream>
#include <ctime>
#include <cstdlib>

#include "Deck.h"
#include "Player.h"

int main()
{
  std::srand(std::time(NULL));
  std::cout << "Battle card game!" << std::endl;

  Deck deck = Deck();

  deck.shuffle();

  Player player1 = Player("First Player");
  Player player2 = Player("Second Player");

  while(!deck.empty()) {
    player1.add(deck.pick());
    player2.add(deck.pick());
  }

  int i = 0;
  while(true) {
    i++;
    std::list<Card> cards;

    Card cardPlayer1, cardPlayer2;

    do {

      if (player1.hadLoose()) {
        std::cout << player1.name() << " had loose" << std::endl;
        return 0;
      }

      if (player2.hadLoose()) {
        std::cout << player2.name() << " had loose" << std::endl;
        return 0;
      }


      cardPlayer1 = player1.pick();
      cardPlayer2 = player2.pick();

      cards.push_back(cardPlayer1);
      cards.push_back(cardPlayer2);

      for(auto it = cards.begin(); it != cards.end(); it++) {
        Card card = *it;
        std::cout << "\t Card: " << card.rank << " of " << card.suite << std::endl;
      }

      if (cardPlayer1.rank > cardPlayer2.rank) {
        std::cout << player1.name() << " had win round " << i << "!" << std::endl;
        player1.add(cards);
        break;
      } else if (cardPlayer1.rank < cardPlayer2.rank) {
        std::cout << player2.name() << " had win round " << i << "!" << std::endl;
        player2.add(cards);
        break;
      }

      if (player1.hadLoose()) {
        std::cout << player1.name() << " had loose" << std::endl;
        return 0;
      }

      if (player2.hadLoose()) {
        std::cout << player2.name() << " had loose" << std::endl;
        return 0;
      }

      std::cout << "BATTLE!!!" << std::endl;
      cards.push_back(player1.pick());
      cards.push_back(player2.pick());

    } while(cardPlayer1.rank == cardPlayer2.rank);
  }
}
