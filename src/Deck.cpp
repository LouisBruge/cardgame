#include <algorithm>
#include "Deck.h"

int randomGen(int);

const int DECK_SIZE = 52;


Deck::Deck()
{
  for(int i = 0; i < DECK_SIZE; i++) {
    Card card {static_cast<RANK>(i % 13), static_cast<SUITE>(i % 4)};
    _cards.push_back(card);
  }
}

std::vector<Card> Deck::cards()
{
  return _cards;
}

void Deck::shuffle()
{
  std::random_shuffle(_cards.begin(), _cards.end(), randomGen);
}

Card Deck::pick()
{
  Card card = _cards.back();
  _cards.pop_back();
  return card;
}

bool Deck::empty()
{
  return _cards.size() == 0;
}

int randomGen (int i)
{
  return std::rand() %i;
}
