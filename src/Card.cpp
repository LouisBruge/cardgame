#include "Card.h"

std::ostream &operator << (std::ostream &o, const  SUITE &s)
{
  switch(s) {
    case SUITE::HEART:
      o << "Heart";
      break;
    case SUITE::CLOVER:
      o << "CLOVER";
      break;
    case SUITE::SPADE:
      o << "SPADE";
      break;
    case SUITE::DIAMONDS:
      o << "DIAMONDS";
      break;
  }
  return o;
}

std::ostream &operator << (std::ostream &o, const RANK &r)
{
  switch(r) {
    case RANK::TWO:
      o << "2";
      break;
    case RANK::THREE:
      o << "3";
      break;
    case RANK::FOUR:
      o << "4";
      break;
    case RANK::FIVE:
      o << "5";
      break;
    case RANK::SIX:
      o << "6";
      break;
    case RANK::SEVEN:
      o << "7";
      break;
    case RANK::HEIGTH:
      o << "8";
      break;
    case RANK::NINE:
      o << "9";
      break;
    case RANK::TEEN:
      o << "10";
      break;
    case RANK::VALET:
      o << "VALET";
      break;
    case RANK::QUEEN:
      o << "QUEEN";
      break;
    case RANK::KING:
      o << "KING";
      break;
    case RANK::ACE:
      o << "ACE";
      break;

  }
  return o;
}
